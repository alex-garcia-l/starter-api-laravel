<?php

return [
    'app' => [
        'Console' => [
            'Commands' => [
                'PermissionRefresh' => [ // app.Console.Commands.PermissionRefresh
                    'done' => 'Se registró correctamente el permiso',
                    'error' => 'Ocurrió un error al registrar el permiso',
                    'success' => 'Se registró correctamente los permisos.'
                ],
            ],
        ],
        'Exceptions' => [ 
            'Handler' => [ // app.Exceptions.Handler
                'ModelNotFoundException' => 'No existe ninguna instancia de :model con el id: :id.',
                'AuthorizationException' => 'No tiene permiso para ejecutar esta acción.',
                'NotFoundHttpException' => 'No se encontró la URL especificada.',
                'MethodNotAllowedHttpException' => 'El método especificado en la petición no es válido.',
                'QueryException' => 'No se puede eliminar de forma permamente el recurso porque está relacionado con algún otro.',
                'unauthenticated' => 'Debes iniciar sesión.',
                'any' => 'Falla inesperada. Intente luego.',
            ],
        ],
        'Http' => [ 
            'Middleware' => [
                'CustomThrottleResponse' => [ // app.Http.Middleware.CustomThrottleResponse
                    'attempts' => 'Demasiados intentos de acceso. Por favor inténtelo de nuevo en :seconds segundos.',
                ],
            ],
        ],
        'Models' => [
            'Address' => [ // app.Models.Address
                'neighborhood' => 'Barrio',
            ]
        ],
    ],
    'resources' => [
        'views' => [
            'index' => [ // resources.views.index
                'message' => 'Usa la aplicación para poder usar la API.',
                'button' => 'Ir a la App'
            ],
        ],
    ],
];