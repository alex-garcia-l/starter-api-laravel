<?php

return [

    'Backoffice' => [
        'Account' => [
            'Auth' => [ // Backoffice.Account.Auth.AuthController
                'AuthController' => [
                    'not-inactive' => 'Usuario inativo.',
                    'unauthorized' => 'No se pudo iniciar sesión.',
                    'success' => 'Se cerró correctamente la sesión.',
                ],
            ],
        ],
        'Administration' => [
            'User' => [
                'AddressController' => [ // Backoffice.Administration.User.AddressController
                    'store' => [
                        'success' => 'Se agregó correctamente la dirección del usuario.',
                        'have-address' => 'El usuario ya cuenta con una dirección agregada.',
                        'error' => 'Ocurrió un error al agregar la dirección del usuario.',
                        'unauthorized' => 'No puedes agregar la dirección del usuario.'
                    ],
                    'show' => [
                        'unauthorized' => 'No puedes ver la dirección del usuario.',
                        'not-have-address' => 'El usuario no tiene una dirección.'
                    ],
                    'update' => [
                        'success' => 'Se actualizó correctamente la dirección del usuario.',
                        'error' => 'Ocurrió un error al actualizar la dirección del usuario.',
                        'unauthorized' => 'No puedes actualizar la dirección del usuario.',
                        'not-have-address' => 'El usuario no tiene una dirección.'
                    ],
                    'destroy' => [
                        'success' => 'Se eliminó correctamente la dirección del usuario.',
                        'error' => 'Ocurrió un error al eliminar la dirección del usuario.',
                        'unauthorized' => 'No puedes aliminar la dirección del usuario.',
                        'not-have-address' => 'El usuario no tiene una dirección.'
                    ],
                ],
                'UserController' => [ // Backoffice.Administration.User.UserController
                    'store' => [
                        'success' => 'Se agregó correctamente el usuario.',
                        'error' => 'Ocurrió un error al agregar el usuario.'
                    ],
                    'show' => [
                        'unauthorized' => 'No puedes ver el usuario.'
                    ],
                    'update' => [
                        'success' => 'Se actualizó correctamente el usuario.',
                        'error' => 'Ocurrió un error al actualizar el usuario.',
                        'unauthorized' => 'No puedes actualizar el usuario.'
                    ],
                    'destroy' => [
                        'success' => 'Se eliminó correctamente el usuario.',
                        'error' => 'Ocurrió un error al eliminar el usuario.'
                    ],
                ],
            ],
        ],
    ],
];