<?php

namespace Database\Factories;

use App\Models\Address;
use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Address::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $latitude = $this->faker->optional(0.70)->latitude();
        return [
            'street' => $this->faker->streetName,
            'neighborhood' => $this->faker->citySuffix,
            'outdoor_number' =>  $this->faker->buildingNumber,
            'interior_number' => $this->faker->optional()->buildingNumber,
            'zip_code' => $this->faker->postcode,
            'country' => $this->faker->country,
            'state' => $this->faker->state,
            'city' => $this->faker->city,
            'between' => $this->faker->optional()->streetName,
            'reference' => $this->faker->optional()->text($maxNbChars = 200),
            'latitude' => $latitude,
            'longitude' => $latitude ? $this->faker->longitude() : null
        ];
    }
}
