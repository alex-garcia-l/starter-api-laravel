<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $role = Role::all()->except(['owner', 'developer'])->random();
        $image = "0" . $this->faker->randomElement($array = array (1, 2, 3, 4, 5, 6)) . ".jpeg";

        return [
            'full_name' => $this->faker->name,
            'username' => $this->faker->unique()->userName,
            'email' => $this->faker->unique()->safeEmail,
            'password' => bcrypt('abcd1234'),
            'phone' => $this->faker->regexify('[0-9]{10}'),
            'profile_image' => $image,
            'status' => $this->faker->randomElement([User::STATUS_ACTIVE, User::STATUS_INACTIVE]),
            'role_name' => $role->name
        ];
    }
}
