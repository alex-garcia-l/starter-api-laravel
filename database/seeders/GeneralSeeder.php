<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /** 
         * Create Owner User
         *
         */
        $user = new User;
        $user->full_name = 'Owner';
        $user->username = 'owner';
        $user->email = 'mail@mail.com';
        $user->password = '$2y$10$YmKslD4WDdHnYbqakc9VMu2J4e8PHZHMLWpHJaEAtmOnF3LKyaPFi'; // secret0
        $user->phone = '0987654321';
        $user->profile_image = 'default.png';
        $user->role()->associate('owner');
        $user->save();

        /**
         * Call seeds
         */

         if (config('app.seed_model')) {
            $this->call([
                UserSeeder::class
            ]);
         }
    }
}
