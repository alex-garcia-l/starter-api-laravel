<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use App\Models\Address;
use App\Models\Permission;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->clearDB();

        collect(['owner', 'admin'])->each(function ($item) {
            $role = new Role;
            $role->name = $item;
            if ($item === 'owner') {
                $role->visible = false;
            }
            $role->save();
        });

        if (\App::environment() == 'local') {
            $this->call([GeneralSeeder::class]);
        }

        \Artisan::call('permission:refresh');
    }

    /*
     * Clean database
     */
    private function clearDB() {
        Role::truncate();
        User::truncate();
        Address::truncate();
        Permission::truncate();
        \DB::table('permission_role')->truncate();
        \DB::table('permission_user')->truncate();
    }
}