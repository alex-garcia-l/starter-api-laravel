<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->string('name')->unique();
            $table->boolean('visible');
        });

        Schema::create('permissions', function (Blueprint $table) {
            $table->string('name')->unique();
            $table->boolean('visible');
        });

        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('full_name');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone', 10);
            $table->string('profile_image')->nullable();
            $table->string('status');
            $table->string('role_name');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('role_name')->references('name')->on('roles');
        });

        Schema::create('permission_role', function (Blueprint $table) {
            $table->string('permission_name');
            $table->string('role_name');

            $table->foreign('permission_name')->references('name')->on('permissions');
            $table->foreign('role_name')->references('name')->on('roles');
        });

        Schema::create('permission_user', function (Blueprint $table) {
            $table->string('permission_name');
            $table->unsignedInteger('user_id');

            $table->foreign('permission_name')->references('name')->on('permissions');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permission_user');
        Schema::dropIfExists('permission_role');
        Schema::dropIfExists('users');
        Schema::dropIfExists('permissions');
        Schema::dropIfExists('roles');
    }
}
