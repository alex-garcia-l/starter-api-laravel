<?php

namespace App\Exceptions;

use App\Traits\ApiResponser;
use Illuminate\Database\QueryException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    use ApiResponser;
    
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof ValidationException) {
            return $this->convertValidationExceptionToResponse($exception, $request);
        }

        if ($exception instanceof ModelNotFoundException) {
            $model = strtolower(class_basename($exception->getModel()));
            $id = $exception->getIds()[0];
            return $this->messageResponse(__('general.app.Exceptions.Handler.ModelNotFoundException', ['model' => $model, 'id' => $id]), 404, 'error');
        }

        if ($exception instanceof AuthenticationException) {
            return $this->unauthenticated($request, $exception);
        }

        if ($exception instanceof AuthorizationException) {
            return $this->messageResponse(__('general.app.Exceptions.Handler.AuthorizationException'), 403, 'error');
        }

        if ($exception instanceof NotFoundHttpException) {
            return $this->messageResponse(__('general.app.Exceptions.Handler.NotFoundHttpException'), 404, 'error');
        }

        if ($exception instanceof MethodNotAllowedHttpException) {
            return $this->messageResponse(__('general.app.Exceptions.Handler.MethodNotAllowedHttpException'), 405, 'error');
        }

        if ($exception instanceof HttpException) {
            return $this->messageResponse($exception->getMessage(), $exception->getStatusCode(), 'error');
        }

        if ($exception instanceof QueryException) {
            $code = $exception->errorInfo[1];

            if ($code == 1451) {
                return $this->messageResponse(__('general.app.Exceptions.Handler.QueryException'), 409, 'error');
            }
        }

        if (config('app.debug')) {
            return parent::render($request, $exception);            
        }

        return $this->messageResponse(__('general.app.Exceptions.Handler.any'), 500, 'error');
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return $this->messageResponse(__('general.app.Exceptions.Handler.unauthenticated'), 401, 'error');
    }

    /**
     * Create a response object from the given validation exception.
     *
     * @param  \Illuminate\Validation\ValidationException  $e
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {
        $errors = $e->validator->errors()->getMessages();

        return $this->objectResponse([
            'error' => $errors
        ], 422, 'error');
    }

}
