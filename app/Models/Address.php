<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'addresses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */
    protected $fillable = [
        'street',
        'neighborhood',
        'outdoor_number',
        'interior_number',
        'zip_code',
        'country',
        'state',
        'city',
        'between',
        'reference',
        'latitude',
        'longitude'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'addressable_id',
        'addressable_type'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        // 
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        // 
    ];

    /**
     * Relationship Belongs To User
     */
    public function addressable()
    {
        return $this->morphTo();
    }

    /**
     * Full Address Attribute
     *
     * @return float
     */
    public function getFullAddressAttribute()
    {
        $number = $this->outdoor_number === "SN" ? "SN" : "#{$this->outdoor_number}";
        if ($this->interior_number) {
            $number .= "-" . $this->interior_number;
        }

        $neighborhood = __('general.app.Models.Address.neighborhood') . " {$this->neighborhood}";

        return "{$this->street} {$number}, {$neighborhood}, CP {$this->zip_code}, {$this->city} {$this->state} {$this->country}";
    }
}
