<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'permissions';

    /**
     * Set the primary key tag.
     *
     * @var string
     */
    protected $primaryKey = 'name';

    /**
     * Indicates if the primary key is increaseable.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'visible' => true,
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'description'
    ];

    /**
     * Relationship Belongs To Many Role
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Relationship Belongs To Many Trace
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        self::updating(function ($model) {
            throw new \Exception('Can not update the permission.');
        });

        self::deleting(function ($model) {
            throw new \Exception('Can not delete the permission.');
        });
    }
}
