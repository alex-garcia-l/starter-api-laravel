<?php

namespace App\Http\Resources\Backoffice\Administration\User\AddressController;

use Illuminate\Http\Resources\Json\JsonResource;

class ShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'street' => $this->street,
            'neighborhood' => $this->neighborhood,
            'outdoor_number' => $this->outdoor_number,
            'interior_number' => $this->interior_number,
            'zip_code' => $this->zip_code,
            'country' => $this->country,
            'state' => $this->state,
            'city' => $this->city,
            'between' => $this->between,
            'reference' => $this->reference,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'full_adrres' => $this->full_address
        ];
    }
}
