<?php

namespace App\Http\Requests\Backoffice\Administration\User\UserController;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'nullable|string|min:5',
            'username' => 'nullable|string|unique:users,username,' . $this->route('user')->id . ',id,deleted_at,NULL',
            'email' => 'nullable|email|unique:users,email,' . $this->route('user')->id . ',id,deleted_at,NULL',
            'password' => 'nullable|string|min:8|confirmed',
            'phone' => 'nullable|digits:10|unique:users,phone,' . $this->route('user')->id . ',id,deleted_at,NULL',
            'role_name' => "nullable|string|exists:roles,name",
        ];
    }
}
