<?php

namespace App\Http\Requests\Backoffice\Administration\User\UserController;

use Illuminate\Foundation\Http\FormRequest;

class IndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'nullable|integer|min:1',
            'per_page' => 'nullable|integer|in:10,15,30,50,100',
            'order_column' => 'nullable|in:null,id,full_name,username,email,phone,profile_image,role_name,status',
            'sort' => 'nullable|in:null,asc,desc',
            'search_column' => 'nullable|in:null,id,full_name,username,email,phone,profile_image,role_name,status',
            'search' => 'nullable|required_with:search_column'
        ];
    }
}
