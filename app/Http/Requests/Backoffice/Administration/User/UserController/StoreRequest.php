<?php

namespace App\Http\Requests\Backoffice\Administration\User\UserController;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required|string|min:5',
            'username' => 'required|string|unique:users,username,NULL,id,deleted_at,NULL',
            'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
            'password' => 'required|string|min:8|confirmed',
            'phone' => 'required|digits:10|unique:users,phone,NULL,id,deleted_at,NULL',
            'role_name' => "required|string|exists:roles,name",
        ];
    }
}
