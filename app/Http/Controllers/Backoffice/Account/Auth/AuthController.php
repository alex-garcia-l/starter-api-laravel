<?php

namespace App\Http\Controllers\Backoffice\Account\Auth;

use App\Models\User;
use App\Http\Requests\Backoffice\Account\Auth\LoginRequest;
use App\Http\Resources\Backoffice\Account\Auth\UserResource;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * User login.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $user = User::where('username', $request->username)->first();

        if ($user->role_name !== 'owner' && $user->status !== User::STATUS_ACTIVE) {
            return $this->messageResponse(__('response.Backoffice.Account.Auth.AuthController.not-inactive'), 401);
        }
        
        if ($token = auth()->guard('api_user')->attempt($request->all())) {
            return $this->respondWithToken($token, auth()->guard('api_user')->user());
        } else {
            return $this->messageResponse(__('response.Backoffice.Account.Auth.AuthController.unauthorized'), 401);
        }
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->guard('api_user')->logout();
        return $this->messageResponse(__('response.Backoffice.Account.Auth.AuthController.success'));
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return $this->responseJsonResource(new UserResource(auth()->guard('api_user')->user()));
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->guard('api_user')->refresh(), auth()->guard('api_user')->user());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token, $user)
    {
        return $this->objectResponse([
            'token_type' => 'bearer',
            'access_token' => $token,
            'expires_in' => auth()->guard('api_user')->factory()->getTTL() * 60
        ]);
    }
}
