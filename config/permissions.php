<?php
// Roles ['manager', 'supervisor', 'agent', 'call_center']
return [
    [
        'name' => 'user-address-store',
        'module' => 'administration',
        'submodule' => 'user',
        'required_permissions' => [
            'user-index',
            'user-show',
        ],
        'roles' => [
            'admin',
        ]
    ],
    [
        'name' => 'user-address-show',
        'module' => 'administration',
        'submodule' => 'user',
        'required_permissions' => [
            'user-index',
            'user-show',
        ],
        'roles' => [
            'admin',
        ]
    ],
    [
        'name' => 'user-address-update',
        'module' => 'administration',
        'submodule' => 'user',
        'required_permissions' => [
            'user-index',
            'user-show',
        ],
        'roles' => [
            'admin',
        ]
    ],
    [
        'name' => 'user-address-destroy',
        'module' => 'administration',
        'required_permissions' => [
            'user-index',
            'user-show',
        ],
        'submodule' => 'user',
        'roles' => [
            'admin',
        ]
    ],

    
    [
        'name' => 'user-index',
        'module' => 'administration',
        'submodule' => 'user',
        'roles' => [
            'admin',
        ]
    ],
    // [
    //     'name' => 'user-list',
    //     'module' => 'administration',
    //     'submodule' => 'user',
    //     'roles' => [
    //         'admin',
    //     ],
    //     'visible' => false
    // ],
    [
        'name' => 'user-store',
        'module' => 'administration',
        'submodule' => 'user',
        'roles' => [
            'admin',
        ]
    ],
    [
        'name' => 'user-show',
        'module' => 'administration',
        'submodule' => 'user',
        'roles' => [
            'admin',
        ]
    ],
    [
        'name' => 'user-update',
        'module' => 'administration',
        'submodule' => 'user',
        'roles' => [
            'admin',
        ]
    ],
    [
        'name' => 'user-destroy',
        'module' => 'administration',
        'submodule' => 'user',
        // 'required_permissions' => [
        //     'user-index'
        // ],
        'roles' => [
            'admin',
        ]
    ],
];